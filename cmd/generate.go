package cmd

import (
	"fmt"
	"gengen/internal/generate"
	"gengen/internal/template"
	"os"
	"path/filepath"

	"github.com/spf13/cobra"
)

// generateCmd represents the generate command
var generateCmd = &cobra.Command{
	Use:   "generate",
	Short: "A short generate",
	Long:  `A longer description generate`,
	Run: func(cmd *cobra.Command, args []string) {
		backend, _ := cmd.Flags().GetString("backend")
		appName, _ := cmd.Flags().GetString("appname")

		if appName == "" || backend == "" {
			fmt.Println("Please provide the AppName using the --appname flag and the Backend using the --backend flag")
			os.Exit(1)
		}

		var baseTemplate string
		switch backend {
		case "golang":
			baseTemplate = template.BaseGolangTemplate
		case "python":
			baseTemplate = template.BasePythonTemplate
		default:
			fmt.Println("Unsupported backend choice")
			os.Exit(1)
		}

		if err := generate.GenerateDirectoryStructure(appName, "applications", baseTemplate); err != nil {
			fmt.Printf("Error generating application directory: %v\n", err)
			os.Exit(1)
		}

		if err := generate.GenerateDirectoryStructure(appName, filepath.Join("overlays", "development"), template.OverlayTemplate); err != nil {
			fmt.Printf("Error generating overlay directory: %v\n", err)
			os.Exit(1)
		}

		fmt.Println("Directories and kustomization.yaml files created successfully.")
	},
}

func init() {
	generateCmd.Flags().StringP("appname", "a", "", "Name of the application")
	generateCmd.Flags().StringP("backend", "b", "", "Backend choice (golang, python, etc.)")
	rootCmd.AddCommand(generateCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// generateCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// generateCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
