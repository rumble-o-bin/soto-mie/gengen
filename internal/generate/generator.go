package generate

import (
	"html/template"
	"os"
	"path/filepath"
)

func GenerateDirectoryStructure(appName, basePath, templateContent string) error {
	currentDir, err := os.Getwd()
	if err != nil {
		return err
	}

	appDir := filepath.Join(currentDir, basePath, appName)
	if err := os.MkdirAll(appDir, 0755); err != nil {
		return err
	}

	tmpl, err := template.New("Template").Parse(templateContent)
	if err != nil {
		return err
	}

	data := struct {
		AppName string
	}{
		AppName: appName,
	}

	filePath := filepath.Join(appDir, "kustomization.yaml")
	file, err := os.Create(filePath)
	if err != nil {
		return err
	}
	defer file.Close()

	if err := tmpl.Execute(file, data); err != nil {
		return err
	}

	return nil
}
