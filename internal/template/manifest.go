package template

const (
	BaseGolangTemplate = `apiVersion: kustomize.config.k8s.io/v1beta1
kind: Kustomization

namePrefix: {{.AppName}}-

commonLabels:
  app: {{.AppName}}

resources:
- github.com/yourusername/kustomize-base-template/base-golang?ref=main
`

	BasePythonTemplate = `apiVersion: kustomize.config.k8s.io/v1beta1
kind: Kustomization

namePrefix: {{.AppName}}-

commonLabels:
  app: {{.AppName}}

resources:
- github.com/yourusername/kustomize-base-template/base-python?ref=main
`

	OverlayTemplate = `apiVersion: kustomize.config.k8s.io/v1beta1
kind: Kustomization

resources:
- ../../../{{.AppName}}

commonLabels:
  env: development
`
)
